# client
Please print on company letterhead

Date:

TO WHOMSOEVER IT MAY CONCERN


Re:  Verification of work and anticipated duration:

Dear Sir or Madam:

This letter is being provided at the request of (“Name of the Petitioner”) to verify the facts stated herein.  All representations made are strictly for submission to the immigration authorities.  No legal or equitable rights are created, modified or abrogated by this letter. 

We confirm that (“Name of the Employee”) is a full time employee of (“Name of the Petitioner”). (“Name of the Employee”) will be based at our offices located at (“Work Location”) as a (“Title”) pursuant to our contract with (“Second Middle Vendor/ Middle Vendor”). Formac Inc. has developed and assigned the following job duties to (“Name of the Employee”)

Job Duties (Provide 5-6 job duties)

We confirm (“Name of the Employee”) will not be our employee.  As long as he follows our standard workplace policies, we will have no responsibility to dictate how he performs his job duties.  We will not be responsible for paying his salary, claim him as an employee for tax purposes and will have no authority to discharge him, alter his duties or terminate his employment. We confirm that (“Name of the Employee”) will remain the employee of (“Name of the Petitioner”) and they will retain ultimate control on his employment.

We anticipate the need for his services for about three years; but this statement is not meant to be a binding contract or agreement.

Regards,


Name and title
